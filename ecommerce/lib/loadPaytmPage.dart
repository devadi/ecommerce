import 'dart:convert';

import 'package:ecommerce/confirmationpage.dart';
import 'package:ecommerce/homepage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;

class LoadPaytmView extends StatefulWidget
{
  String id;
  String amount;
  var email;
  var phone;
  LoadPaytmView({this.id,this.amount,this.email,this.phone});
  @override
  _LoadPaytmView  createState() => _LoadPaytmView(orderId:id,grandTotal:amount,email:email,phone:phone);
}

class _LoadPaytmView extends State<LoadPaytmView> {

  WebViewController _webController;
  String orderId;
  String grandTotal;
  var email;
  var phone;
  bool _loadingPayment = true;
  Map res = new Map();
    String url = 'http://100.27.12.26:5000/api/Paytm/payByPaytm';
  String baseUrl = "http://100.27.12.26:5000/api";
  String status = "";
  String trasactionId = "";
  String orderStatus = "";




  PePaytmStatus _responseStatus = PePaytmStatus.PAYMENT_LOADING;

  _LoadPaytmView({this.orderId,this.grandTotal,this.email,this.phone});
  String _loadHTML() {
    return "<html> <body onload='document.f.submit();'> <form id='f' name='f' method='get' action='${url}'> <input type='hidden' name='orderID' value='ORDER_${orderId}'/> <input  type='hidden' name='custID' value='9871' /> <input  type='hidden' name='amount' value='${grandTotal}' /> <input type='hidden' name='custEmail' value='${email}' /> <input type='hidden' name='custPhone' value='${phone}' /> </form> </body> </html>";
  }

  void getData() {
    _webController.evaluateJavascript("document.body.innerText").then((data) {
      print(data);
      var decodedJSON = jsonDecode(data);
      print(decodedJSON);
      Map responseJSON = json.decode(decodedJSON);
      final checksumResult = responseJSON["status"];
      print(checksumResult);
      print('dgafstgfhfd');
      final paytmResponse = responseJSON["data"];
      trasactionId = paytmResponse['TXNID'];
      orderStatus = paytmResponse['STATUS'];
      if (paytmResponse["STATUS"] == "TXN_SUCCESS") {
        if (checksumResult == "1") {
          _responseStatus = PePaytmStatus.PAYMENT_SUCCESSFUL;
          status = 'Order Placed';
          updatetransaction();
        } else {
          _responseStatus = PePaytmStatus.PAYMENT_CHECKSUM_FAILED;
          status = 'Order Pending';
          updatetransaction();
        }
      } else if (paytmResponse["STATUS"] == "TXN_FAILURE") {
        _responseStatus = PePaytmStatus.PAYMENT_FAILED;
        status = 'Order Rejected';
        updatetransaction();
      }
      this.setState(() {});
    });
  }

  Future<Map<String, dynamic>> updatetransaction() async {
    String myUrl = "$baseUrl/Order/orderTransaction";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          "trasactionId":trasactionId,
          "orderStatus":orderStatus,
          "status":status,
          "orderId":orderId
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
    } else {
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Widget getResponseScreen() {
    switch (_responseStatus) {
      case PePaytmStatus.PAYMENT_SUCCESSFUL:
        return PaymentSuccessfulScreen();
      case PePaytmStatus.PAYMENT_CHECKSUM_FAILED:
        return CheckSumFailedScreen();
      case PePaytmStatus.PAYMENT_FAILED:
        return PaymentFailedScreen();
      default:
        return PaymentSuccessfulScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: WebView(
                  debuggingEnabled: false,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (controller) {
                    _webController = controller;
                    _webController.loadUrl(
                        new Uri.dataFromString(_loadHTML(), mimeType: 'text/html')
                            .toString());
                  },
                  onPageFinished: (page) {
                    if (page.contains("/process")) {
                      if (_loadingPayment) {
                        this.setState(() {
                          _loadingPayment = false;
                        });
                      }
                    }
                    if (page.contains("/paymentReceipt")) {
                      getData();
                    }
                  },
                ),
              ),
              (_loadingPayment)
                  ? Center(
                child: CircularProgressIndicator(),
              )
                  : Center(),
              (_responseStatus != PePaytmStatus.PAYMENT_LOADING)
                  ? Center(child: getResponseScreen())
                  : Center()
            ],
          )),
    );
  }
}

enum PePaytmStatus {
  PAYMENT_LOADING,
  PAYMENT_SUCCESSFUL,
  PAYMENT_PENDING,
  PAYMENT_FAILED,
  PAYMENT_CHECKSUM_FAILED
}

class PaymentSuccessfulScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Great!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Thank you making the payment!",
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                  color: Colors.black,
                  child: Text(
                    "Close",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ConfirmPage()),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}

class PaymentFailedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "OOPS!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Payment was not successful, Please try again Later!",
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                  color: Colors.black,
                  child: Text(
                    "Close",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyHomePage()),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}

class CheckSumFailedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(15.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Oh Snap!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Problem Verifying Payment, If you balance is deducted please contact our customer support and get your payment verified!",
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                  color: Colors.black,
                  child: Text(
                    "Close",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyHomePage()),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}