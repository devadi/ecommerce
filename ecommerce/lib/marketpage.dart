import 'dart:convert';
import 'package:ecommerce/shopdetail.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class MarketPage extends StatefulWidget {
  @override
  _MarketPageState createState() => _MarketPageState();
}

class _MarketPageState extends State<MarketPage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    getShopList();
  }

  Future<Map<String, dynamic>> getShopList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/Shop/shopList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    print(value.length);
    return new Scaffold(

      body: new Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                "Market",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i) {
                    return new GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ShopDetailPage(name:value[i]['shop_name'],image:value[i]['shop_image'],address:value[i]['shop_address'],phone:value[i]['shop_phoneNo'])
                          ),
                        );
                      },
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Image.network(
                              'http://100.27.12.26:5000/resources/' +
                                  value[i]['shop_image'],
                              height: 130,
                              width: 130,
                              fit: BoxFit.cover,
                            ),
                            Text('${value[i]['shop_name']}'),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
