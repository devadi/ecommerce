import 'dart:convert';

import 'package:ecommerce/addresslist.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class NewAddressPage extends StatefulWidget {
  @override
  NewAddressPageState createState() => NewAddressPageState();
}

class NewAddressPageState extends State<NewAddressPage> {
  TextEditingController _houseNo = new TextEditingController();
  TextEditingController _roadNo = new TextEditingController();
  TextEditingController _address = new TextEditingController();
  TextEditingController _city = new TextEditingController();
  TextEditingController _postalCode = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
        }
        else
        {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;

      }
    });
  }

  Future<Map<String, dynamic>> createNewAddress() async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/Address/addAddress";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          "uid": uid.toString(),
          "houseNo":_houseNo.text,
          "roadNo":_roadNo.text,
          "addressLane":_address.text,
          "city":_city.text,
          "postalCode":_postalCode.text
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson[',message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => AddressListPage()),
      );
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: new Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                new Container(
                  height: 30.0,
                  alignment: Alignment.topLeft,
                  child: new Text(
                    "Create Address",
                    style: new TextStyle(fontSize: 26.0),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'House No',
                      hintText: 'Enter your House No'
                  ),
                  keyboardType: TextInputType.text,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter your House No";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _houseNo,
                ),
                SizedBox(height: 10),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'Road No',
                      hintText: 'Enter your Road No'
                  ),
                  keyboardType: TextInputType.text,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter Road No";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _roadNo,
                ),
                SizedBox(height: 10),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'Address lane',
                      hintText: 'Enter your address lane'
                  ),
                  keyboardType: TextInputType.text,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter address lane";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _address,
                ),
                SizedBox(height: 10),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'City',
                      hintText: 'Enter your city'
                  ),
                  keyboardType: TextInputType.text,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter city";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _city,
                ),
                SizedBox(height: 10),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'Postal Code',
                      hintText: 'Enter your Postal code'
                  ),
                  keyboardType: TextInputType.number,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter Postal code";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _postalCode,
                ),
                SizedBox(height: 30.0),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        createNewAddress();
                      }
                    },
                    padding: const EdgeInsets.all(0.0),
                    textColor: Colors.white,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF0D47A1),
                            Color(0xFF42A5F5),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                      child: const Text('Continue',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20)),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

}