
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class ChangePassPage extends StatefulWidget {
  @override
  ChangePassPageState createState() => ChangePassPageState();
}

class ChangePassPageState extends State<ChangePassPage> {


  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String baseUrl = "http://100.27.12.26:5000/api";
  final _opassword = TextEditingController();
  final _npassword = TextEditingController();
  final _cpassword = TextEditingController();
  bool _obscureText = true;
  bool _obscureText1 = true;
  bool _obscureText2 = true;
  ProgressDialog pr;

  Future<Map<String, dynamic>> changePassword() async {

    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/User/forgetPasswordGetOTP";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body: {"Email": ''
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);

    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      _opassword.clear();
      _npassword.clear();
      _cpassword.clear();
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
     /* Navigator.pop(context);
      Navigator.push(context,
        MaterialPageRoute(builder: (context) => ForgetOtpScreen(otp:otp,email:email,password:password)),
      );*/
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
            builder: (BuildContext context){
              return IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.grey,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            }),
      ),
        body: Form(
          key: _formKey,
          child: Container(
            child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[

                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(bottom: 15.0),
                        child: Text(
                          'Change Password',

                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Roboto",
                              fontSize: 20),
                        )),
                    SizedBox(height: 40.0),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                    ),

                    Align(
                      alignment: Alignment.center,
                      child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(10),
                          child: Text(
                            'Create a new and Unique Password for your account',
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey),
                          )),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'Current Password',
                        hintText: 'Enter your Current password',
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                          child: Icon(_obscureText
                              ? Icons.visibility_off
                              : Icons.visibility),
                        ),
                      ),
                      validator: (val) {
                        if (val.length == 0) {
                          return "Please enter your current password";
                        } else if (val.length < 6) {
                          return "Password too short";
                        } else {
                          return null;
                        }
                      },
                      controller: _opassword,
                      obscureText: _obscureText,
                    ),

                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'New Password',
                        hintText: 'Enter your New password',
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText1 = !_obscureText1;
                            });
                          },
                          child: Icon(_obscureText1
                              ? Icons.visibility_off
                              : Icons.visibility),
                        ),
                      ),
                      validator: (val) {
                        if (val.length == 0) {
                          return "Please enter your New password";
                        } else if (val.length < 6) {
                          return "Password too short";
                        } else {
                          return null;
                        }
                      },
                      controller: _npassword,
                      obscureText: _obscureText1,
                    ),

                    SizedBox(
                      height: 10.0,
                    ),


                    TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'Confirm Password',
                        hintText: 'Re Enter your password',
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText2 = !_obscureText2;
                            });
                          },
                          child: Icon(_obscureText2
                              ? Icons.visibility_off
                              : Icons.visibility),
                        ),
                      ),
                      validator: (val) {
                        if (val.length == 0) {
                          return "Please re enter password";
                        } else if(val!=_npassword.text)
                        {
                          return "Password not matched";
                        } else {
                          return null;
                        }
                      },
                      controller: _cpassword,
                      obscureText: _obscureText2,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),

                    RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
//                          changePassword();
                        }
                      },
                      padding: const EdgeInsets.all(0.0),
                      textColor: Colors.white,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color(0xFF0D47A1),
                              Color(0xFF42A5F5),
                            ],
                          ),
                        ),
                        padding: const EdgeInsets.all(10.0),
                        child: const Text(
                            'Continue',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 20)
                        ),
                      ),
                    ),
                  ],
                )),
    ),
        ),
    );
  }

}