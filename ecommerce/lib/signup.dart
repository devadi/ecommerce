import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ecommerce/login.dart';
import 'package:ecommerce/otppage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class SignupPage extends StatefulWidget {


  @override
  SignupPageState createState() => SignupPageState();
}

class SignupPageState extends State<SignupPage> {

  String baseUrl = "http://100.27.12.26:5000/api";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _name = TextEditingController();
  final _email = TextEditingController();
  final _phone = TextEditingController();
  final _password = TextEditingController();
  bool _obscureText = true;
  ProgressDialog pr;
  Map<String,dynamic> value;
  SharedPreferences sharedPreferences;
  bool checkValue = false;
  String uid,otp;


  _onChange(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = value;
      sharedPreferences.setBool("check", checkValue);
      sharedPreferences.setString("uid",uid.toString());
      sharedPreferences.commit();
    });
  }


  Future<Map<String, dynamic>> signupUser() async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/User/userSignup";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          "Name": _name.text,
          "Email":_email.text.trim(),
          "Password":_password.text,
          "Phone":_phone.text
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      uid = value['uid'].toString();
      otp = value['otp'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);

      _name.clear();
      _email.clear();
      _password.clear();
      setState(() {
        _onChange(true);
      });
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
      Navigator.pop(context);
      Navigator.push(context,
        MaterialPageRoute(builder: (context) => OtpScreen(otp:otp,uid:uid)),
      );
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
            builder: (BuildContext context){
              return IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.grey,
                ),
                onPressed: () {
                },
              );
            }),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: new Container(
            padding: EdgeInsets.only(left: 15.0,right: 15.0),
            child: new Column(
              textDirection: TextDirection.ltr,
              children: <Widget>[
                SizedBox(
                  height: 35.0,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text("Signup",
                    textAlign: TextAlign.start,
                    style: new TextStyle(
                        fontSize: 26.0
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'Name',
                      hintText: 'Enter your Name'
                  ),
                  keyboardType: TextInputType.text,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter your Name";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _name,
                ),
                const SizedBox(height: 20),

                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'Email',
                      hintText: 'Enter your Email Id'
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (val){
                    var emailReg = RegExp(
                        r"[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?");
                    if(val.length == 0)
                    {
                      return "Please enter email";
                    }
                    else if(!emailReg.hasMatch(val))
                    {
                      return 'Please enter valid email address';
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _email,
                ),
                const SizedBox(height: 20),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      counterText: "",
                      labelText: 'Phone',
                      hintText: 'Enter your Phone Number'
                  ),
                  keyboardType: TextInputType.phone,
                  maxLength: 10,
                  validator: (val){
                    if(val.length == 0)
                    {
                      return "Please enter your phone number";
                    }
                    else if(val.length!=10)
                      {
                        return "Please enter valid phone number";
                      }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _phone,
                ),
                const SizedBox(height: 20),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    hintText: 'Enter your password',
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _obscureText = !_obscureText;
                        });
                      },
                      child: Icon(_obscureText ? Icons.visibility_off : Icons.visibility),
                    ),
                  ),
                  validator: (val)
                  {
                    if(val.length == 0)
                    {
                      return "Please enter password";
                    }
                    else if(val.length < 6)
                    {
                      return "Password too short";
                    }
                    else
                    {
                      return null;
                    }
                  },
                  controller: _password,
                  obscureText: _obscureText,
                ),
                const SizedBox(height: 20),
                RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      signupUser();
                    }
                  },
                  padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFF0D47A1),
                          Color(0xFF42A5F5),
                        ],
                      ),
                    ),
                    padding: const EdgeInsets.all(10.0),
                    child: const Text(
                        'Sign Up',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20)
                    ),

                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Don\'\t have an account? ',
                        style: TextStyle(
                            color: Colors.grey
                        ),
                      ),
                      GestureDetector(
                        child: Text(
                          'Login',
                          style: TextStyle(
                              color: Colors.black
                          ),
                        ),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()),);
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }

}

