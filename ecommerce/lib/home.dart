import 'dart:convert';
import 'package:ecommerce/categorypage.dart';
import 'package:ecommerce/productdetail.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];
  List<dynamic> data = [];
  List<dynamic> subData = [];
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    getCategoryList();
  }

  Future<Map<String, dynamic>> getCategoryList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/category/categoryList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      data = parsedJson['data'];
    });
    print(data);
    getProductList();
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> getSubCategoryList(String cid)  async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/SubCategory/subCategoryList";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:{'cid': cid.toString()}
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {

      subData = parsedJson['data'];
    });
    print(subData);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> getProductList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/product/getProductList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }
  Future<Map<String, dynamic>> getProductListByCid(String cid) async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/product/getProductByCategory";
    print(myUrl);
    final response = await http.post(
      myUrl,
      headers: {'Accept': 'application/json'},
      body: {
        'catId':cid
      },
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    print(value.length);
    Widget horizontalList1 = new Container(
      margin: EdgeInsets.symmetric(vertical: 15.0),
      height: 80.0,
      child: new ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: data.length,
        itemBuilder: (BuildContext context, int i) {
          return new GestureDetector(
            onTap: () {
              print(data[i]['cid']);
              getSubCategoryList(data[i]['cid'].toString());
            },
            child: new Container(
              width: 130.0,
              child: new Card(
                child: data == [''] ||  data[i]['C_Image'] == null
                ? Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            'http://style.anu.edu.au/_anu/4/images/placeholders/person.png'),
                        fit: BoxFit.fitWidth),
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  ),
                  child: Center(
                    child: Text('${data[i]['C_Name']}'),
                  ),
                )
                :Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            'http://100.27.12.26:5000/resources/' +
                                data[i]['C_Image']),
                        fit: BoxFit.fitWidth),
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  ),
                  child: Center(
                    child: Text('${data[i]['C_Name']}'),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
    Widget horizontalList2 = new Container(
      margin: EdgeInsets.symmetric(vertical: 5.0),
      height: 50.0,
      child: new ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: subData.length,
        itemBuilder: (BuildContext context, int i) {
          return new GestureDetector(
            onTap: () {
              print(subData[i]['cid']);
              getProductListByCid(subData[i]['cid']);
            },
            child: new Container(
              margin: EdgeInsets.only(left: 5.0,right: 20.0),
              child: new Chip(
                label: Text(
                    '${subData[i]['SC_Name']}',
                  style: TextStyle(
                    color: Colors.white
                  ),
                ),
                backgroundColor: Color(0xFF0D47A1),
              ),
            ),
          );
        },
      ),
    );
    return new Scaffold(
      body: new Container(
        padding: EdgeInsets.only(left: 15.0, top: 10.0, right: 15.0),
        child: new Column(
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Container(
                  child: Text(
                    'Categories',
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => CategoryPage(),
                            ),
                          );
                        },
                        child: Text(
                          'See all',
                          style: new TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            horizontalList1,
            horizontalList2,
            /*Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: new Radio(
                            value: 1,
                            groupValue: null,
                            onChanged: null,
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Container(
                          child: Text('Filter'),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Icon(Icons.keyboard_arrow_down),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: new Radio(
                            value: 1,
                            groupValue: null,
                            onChanged: null,
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Container(
                          child: Text('Sort'),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Icon(Icons.keyboard_arrow_down),
                      ],
                    ),
                  ),
                ],
              ),
            ),*/
            SizedBox(
              height: 20.0,
            ),
            new Expanded(
              child: new Container(
                child: new GridView.builder(
                  itemCount: value.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemBuilder: (BuildContext context, int i) {
                    return new GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ProductDetailPage(data: value[i])),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: 20.0, right: 20.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            value == [''] || value[i]['product_image'] == null
                            ? Image.network(
                              'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                            )
                            :Image.network(
                              'http://100.27.12.26:5000/resources/' +
                                  value[i]['product_image'],
                            ),
                            SizedBox(
                              height: 4.0,
                            ),
                            Text('\$${value[i]['product_offerprice']}'),
                            SizedBox(
                              height: 4.0,
                            ),
                            Text('${value[i]['product_name']}'),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
