import 'dart:convert';
import 'package:ecommerce/homepage.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class ProfilePage extends StatefulWidget {
  @override
  ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  SharedPreferences sharedPreferences;
  bool checkValue;
  var userid,
      name,
      gender,
      email,
      phone_no,
      City,
      Address,
      isSocial,
      socialId,
      status;
  bool _isVisible = false;
  final _formkey = GlobalKey<FormState>();
  TextEditingController _name = new TextEditingController();
  TextEditingController _address = new TextEditingController();
  TextEditingController _city = new TextEditingController();
  TextEditingController _gender = new TextEditingController();
  TextEditingController _email = new TextEditingController();
  TextEditingController _phone = new TextEditingController();
  ProgressDialog pr;
  Map<String, dynamic> value;
  SharedPreferences sharedPreferences1;
  String selectedValue = "";
  List<String> cities = [];
  List<DropdownMenuItem> items = [];

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  void showEditText() {
    setState(() {
      _isVisible = true;
    });
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          userid = sharedPreferences.getString("userid");
          name = sharedPreferences.getString("name");
          gender = sharedPreferences.getString("gender");
          email = sharedPreferences.getString("email_ID");
          phone_no = sharedPreferences.getString("phone_no");
          City = sharedPreferences.getString("City");
          Address = sharedPreferences.getString("Address");
          isSocial = sharedPreferences.getString("isSocial");
          socialId = sharedPreferences.getString("socialId");
          status = sharedPreferences.getString("status");

          _name = TextEditingController(text: name);
          _address = TextEditingController(text: Address);
          _city = TextEditingController(text: City);
          _gender = TextEditingController(text: gender);
          _email = TextEditingController(text: email);
          _phone = TextEditingController(text: phone_no);

          getCities();
        } else {
          userid.clear();
          name.clear();
          gender.clear();
          email.clear();
          phone_no.clear();
          City.clear();
          Address.clear();
          isSocial.clear();
          socialId.clear();
          status.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  Future<Map<String, dynamic>> getCities() async {
    final response = await http.get(
        'https://indian-cities-api-nocbegfhqg.now.sh/cities',
        headers: {'Accept': 'application/json'});
    var res = json.decode(response.body);

    for (int i = 0; i < res.length; i++) {
      print(res[i]['City']);
      items.add(
        DropdownMenuItem(
          child: Text(res[i]['City']),
          value: res[i]['City'],
        ),
      );
    }
//    print(items);
  }

  Future<Map<String, dynamic>> updateProfile() async {
    print(_phone.text.trim());
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/User/editUserProfile";
    print(myUrl);
    final response = await http.post(myUrl, headers: {
      'Accept': 'application/json'
    }, body: {
      "Name": _name.text,
      "Email": _email.text.trim(),
      "Phone": _phone.text.trim(),
      "Gender": _gender.text,
      "City": selectedValue,
      "Address": _address.text,
      "uid": userid.toString()
    });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);

      userid = parsedJson['data']['uid'].toString() ?? '';
      name = parsedJson['data']['Name'] ?? '';
      email = parsedJson['data']['Email'] ?? '';
      phone_no = parsedJson['data']['Phone'] ?? '';
      gender = parsedJson['data']['Gender'] ?? '';
      City = parsedJson['data']['City'] ?? '';
      Address = parsedJson['data']['Address'] ?? '';
      isSocial = parsedJson['data']['isSocial'] ?? '';
      socialId = parsedJson['data']['socialId'] ?? '';
      status = parsedJson['data']['status'] ?? '';
      setState(() {
        _onChange(true, value);
      });
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MyHomePage()),
      );
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  _onChange(bool value, Map<String, dynamic> response) async {
    sharedPreferences1 = await SharedPreferences.getInstance();
    setState(() {
      checkValue = value;
      sharedPreferences1.setBool("check", checkValue);
      sharedPreferences1.setString("userid", userid);
      sharedPreferences1.setString("name", name);
      sharedPreferences1.setString("gender", gender);
      sharedPreferences1.setString("email_ID", email);
      sharedPreferences1.setString("phone_no", phone_no);
      sharedPreferences1.setString("City", City);
      sharedPreferences1.setString("Address", Address);
      sharedPreferences1.setString("isSocial", isSocial);
      sharedPreferences1.setString("socialId", socialId);
      sharedPreferences1.setString("status", status);
      print(userid);
      sharedPreferences1.commit();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: new Form(
        key: _formkey,
        child: new SingleChildScrollView(
          child: new Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: <Widget>[
                      new Container(
                        height: 50.0,
                        alignment: Alignment.topLeft,
                        child: new Text(
                          "Profile",
                          style: new TextStyle(fontSize: 26.0),
                        ),
                      ),
                      (_isVisible == false)
                          ? Expanded(
                              child: Align(
                                alignment: Alignment.topRight,
                                child: new Container(
                                  height: 50.0,
                                  child: GestureDetector(
                                    onTap: () {
                                      showEditText();
                                    },
                                    child: Icon(
                                      Icons.edit,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Center(),
                    ],
                  ),
                ),
                (_isVisible == false)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Name',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 14.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            '${name}',
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          )
                        ],
                      )
                    : TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: 'Name', hintText: 'Enter your Name'),
                        keyboardType: TextInputType.text,
                        validator: (val) {
                          if (val.length == 0) {
                            return "Please enter your name";
                          } else {
                            return null;
                          }
                        },
                        controller: _name,
                      ),
                SizedBox(
                  height: 20.0,
                ),
                (_isVisible == false)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Address lane',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 14.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            '${Address}',
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          ),
                        ],
                      )
                    : TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: 'Address lane',
                            hintText: 'Enter your address'),
                        keyboardType: TextInputType.text,
                        validator: (val) {
                          if (val.length == 0) {
                            return "Please enter your address";
                          } else {
                            return null;
                          }
                        },
                        controller: _address,
                      ),
                SizedBox(
                  height: 20.0,
                ),
                (_isVisible == false)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'City',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 14.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            '${City}',
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          ),
                        ],
                      )
                    : SearchableDropdown.single(
                        items: items,
                        value: selectedValue,
                        hint: "Select City",
                        searchHint: "Select one",
                        onChanged: (value) {
                          setState(() {
                            selectedValue = value;
                          });
                        },
                        isExpanded: true,
                      ),
                /*TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: 'City', hintText: 'Enter your city'),
                        keyboardType: TextInputType.text,
                        validator: (val) {
                          if (val.length == 0) {
                            return "Please enter your city";
                          } else {
                            return null;
                          }
                        },
                        controller: _city,
                      ),*/
                SizedBox(
                  height: 20.0,
                ),
                (_isVisible == false)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Gender',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 14.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            '${gender}',
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          ),
                        ],
                      )
                    : TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: 'Gender', hintText: 'Enter your gender'),
                        keyboardType: TextInputType.text,
                        validator: (val) {
                          if (val.length == 0) {
                            return "Please enter your Gender";
                          } else {
                            return null;
                          }
                        },
                        controller: _gender,
                      ),
                SizedBox(
                  height: 20.0,
                ),
                (_isVisible == false)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Email',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 14.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            '${email}',
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          ),
                        ],
                      )
                    : TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: 'Email',
                            hintText: 'Enter your Email Id'),
                        keyboardType: TextInputType.emailAddress,
                        validator: (val) {
                          var emailReg = RegExp(
                              r"[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?");
                          if (val.length == 0) {
                            return "Please enter your email";
                          } else if (!emailReg.hasMatch(val)) {
                            return 'Please enter valid email address';
                          } else {
                            return null;
                          }
                        },
                        controller: _email,
                      ),
                SizedBox(
                  height: 20.0,
                ),
                (_isVisible == false)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Phone Number',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 14.0),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            '${phone_no}',
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          ),
                        ],
                      )
                    : TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                            labelText: 'Phone',
                            hintText: 'Enter your Phone Number'),
                        keyboardType: TextInputType.phone,
                        maxLength: 10,
                        validator: (val) {
                          print(val);
                          if (val.length == 0) {
                            return "Please enter your phone number";
                          } else if (val.length != 10) {
                            return "Please enter valid phone number";
                          } else {
                            return null;
                          }
                        },
                        controller: _phone,
                      ),
                SizedBox(
                  height: 30.0,
                ),
                (_isVisible == false)
                    ? Center()
                    : Align(
                        alignment: Alignment.bottomCenter,
                        child: RaisedButton(
                          onPressed: () {
                            if (_formkey.currentState.validate()) {
                              if(selectedValue !="") {
                                updateProfile();
                              }
                              else
                                {
                                  Toast.show("Please select city", context,
                                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                }
                            }
                          },
                          padding: const EdgeInsets.all(0.0),
                          textColor: Colors.white,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xFF0D47A1),
                                  Color(0xFF42A5F5),
                                ],
                              ),
                            ),
                            padding: const EdgeInsets.all(10.0),
                            child: const Text('UPDATE PROFILE',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20)),
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
