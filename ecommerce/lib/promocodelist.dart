import 'dart:convert';

import 'package:ecommerce/checkoutpage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class PromoCodePage extends StatefulWidget {
  @override
  PromoCodePageState createState() => PromoCodePageState();
}

class PromoCodePageState extends State<PromoCodePage> {

  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];


  @override
  void initState() {
    super.initState();
    promoCodeList();
  }

  Future<Map<String, dynamic>> promoCodeList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/Promo/promoList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                'Promo Code',
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                child: ListView.builder(
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i) {
                    return new Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '${value[i]['code']}',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(context, MaterialPageRoute(
                                              builder: (context) => CheckOutPage(data:value[i],flag:'promo')),);
                                        },
                                        child: Text(
                                          'Apply',
                                          style: new TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.grey
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            child: Text('${value[i]['description']}',
                            style: TextStyle(
                              fontSize: 14.0
                            ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
