import 'dart:convert';
import 'package:ecommerce/subcategory.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';

class CategoryPage extends StatefulWidget {
  @override
  CategoryPageState createState() => CategoryPageState();
}

class CategoryPageState extends State<CategoryPage> {

  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];
  Future<Map<String, dynamic>> getCategoryList() async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/category/categoryList";
    print(myUrl);
    final response = await http.get(myUrl,
        headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    print(value);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  void initState() {
    super.initState();
    getCategoryList();
  }

  @override
  Widget build(BuildContext context) {
    print(value.length);
    print('khghgkhkfk');
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 200.0),
        child: Column(
          children: <Widget>[
            new Container(
              child: new Text(
                "Categories",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                padding: EdgeInsets.only(left: 20.0,right: 20.0),
                height: 150.0,
                child: new GridView.builder(

                  itemCount: value.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                  itemBuilder: (BuildContext context, int i){
                    return new GestureDetector(
                      onTap: (){
                        Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SubCategoryPage(data:value[i])),
                        );
                      },
                      child: new Container(
                          height: 130,
                          child: new Card(
                            child: Container(
                              height: 100.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(image: NetworkImage('http://100.27.12.26:5000/resources/'+value[i]['C_Image']),
                                    fit: BoxFit.fitWidth)
                              ),
                              child: Center(
                                child: Text('${value[i]['C_Name']}'),
                              ),
                            )
                          )
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}