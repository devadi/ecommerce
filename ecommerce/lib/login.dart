import 'dart:convert';
import 'dart:math';
import 'package:ecommerce/home.dart';
import 'package:http/http.dart' as http;
import 'package:ecommerce/forgetpassword.dart';
import 'package:ecommerce/homepage.dart';
import 'package:ecommerce/signup.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _email = TextEditingController();
  final _password = TextEditingController();
  bool _obscureText = true;
  ProgressDialog pr;
  var profileData;
  var facebookLogin = FacebookLogin();
  bool isLoggedIn = false;
  bool isLoading = false;
  Map<String,dynamic> value;
  static const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  SharedPreferences sharedPreferences1;
  bool checkValue = false;
  String userid,name,gender,email,phone_no,City,Address,isSocial,socialId,status;



  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Future<Map<String, dynamic>> loginUser() async {
    print(_email.text.trim());
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/User/userLogin";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          "Email": _email.text.trim(),
          "Password": _password.text
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      _email.clear();
      _password.clear();
      userid=parsedJson['data']['uid'].toString()?? '';
      name=parsedJson['data']['Name']?? '';
      email=parsedJson['data']['Email']?? '';
      phone_no=parsedJson['data']['Phone']?? '';
      gender=parsedJson['data']['Gender'] ??'';
      City=parsedJson['data']['City'] ??'';
      Address=parsedJson['data']['Address']?? '';
      isSocial=parsedJson['data']['isSocial']?? '';
      socialId=parsedJson['data']['socialId']?? '';
      status=parsedJson['data']['status']?? '';
      setState(() {
        _onChange(true, value);
      });
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
      Navigator.of(context).pushReplacementNamed('/Home');
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  _onChange(bool value, Map<String, dynamic> response) async {
    sharedPreferences1 = await SharedPreferences.getInstance();
    setState(() {
      checkValue = value;
      sharedPreferences1.setBool("check", checkValue);
      sharedPreferences1.setString("userid", userid);
      sharedPreferences1.setString("name", name);
      sharedPreferences1.setString("gender", gender);
      sharedPreferences1.setString("email_ID", email);
      sharedPreferences1.setString("phone_no", phone_no);
      sharedPreferences1.setString("City", City);
      sharedPreferences1.setString("Address", Address);
      sharedPreferences1.setString("isSocial", isSocial);
      sharedPreferences1.setString("socialId", socialId);
      sharedPreferences1.setString("status", status);
      print(userid);
      sharedPreferences1.commit();
    });
  }

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      isLoading = false;
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }

  void initiateFacebookLogin() async {
    setState(() {
      isLoading = true;
    });
//    var facebookLoginResult = await facebookLogin.logInWithReadPermissions(['email']);
    var facebookLoginResult = await facebookLogin.logIn(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.cancelledByUser:
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.loggedIn:
        var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult.accessToken.token}');

        var profile = json.decode(graphResponse.body);
        print(profile.toString());
        getFacebookData(profile['name'],profile['id'],profile['email'],"facebook");

        onLoginStatusChanged(true, profileData: profile);
        break;
    }
  }

  Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    final FirebaseUser user = await _auth.signInWithCredential(credential);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    print(user);
    print("google");
    getFacebookData(user.displayName,user.uid,user.email,"google");

    return 'signInWithGoogle succeeded: $user';
  }

  void signOutGoogle() async{
    await googleSignIn.signOut();

    print("User Sign Out");
  }

  Future<Map<String, dynamic>> getFacebookData(String nam,String id,String mail,String issocial) async {

    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    // print(token);
    var code = new Random().nextInt(900000000) + 100000000;
    String myUrl = "$baseUrl/User/socialLogin";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          "Name": nam,
          "password": getRandomString(10),
          "Email": mail,
          "Phone": '9'+code.toString(),
          "socialId":id.toString(),
          "isSocial" : issocial
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    value = parsedJson['data'];
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      //_onChanged(nameController.text);
      setState(() {
        if(issocial == 'google')
        {
          signOutGoogle();
        }
        else if(issocial == 'facebook')
        {
          facebookLogin.logOut();
          setState(() {
            isLoggedIn = false;
          });
        }
      });

      userid=parsedJson['data']['uid'].toString()?? '';
      name=parsedJson['data']['Name']?? '';
      email=parsedJson['data']['Email']?? '';
      phone_no=parsedJson['data']['Phone']?? '';
      gender=parsedJson['data']['Gender'] ??'';
      City=parsedJson['data']['City'] ??'';
      Address=parsedJson['data']['Address']?? '';
      isSocial=parsedJson['data']['isSocial']?? '';
      socialId=parsedJson['data']['socialId']?? '';
      status=parsedJson['data']['status']?? '';
      setState(() {
        _onChange(true, value);
      });
      Navigator.of(context).pushReplacementNamed('/Home');
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);

    }
    return parsedJson;
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey,
            ),
            onPressed: () {
              },
          );
        }),
      ),
      body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.only(left: 15.0, right: 15.0),
              child: new Column(
                children: <Widget>[
                  SizedBox(
                    height: 35.0,
                  ),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "Login",
                      style: new TextStyle(fontSize: 26.0),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                        labelText: 'Email/Number',
                        hintText: 'Enter your Email Id/Phone Number'),
                    keyboardType: TextInputType.emailAddress,
                    validator: (val) {
                      if (val.length == 0) {
                        return "Please enter email/phone Number";
                      } else {
                        return null;
                      }
                    },
                    controller: _email,
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      hintText: 'Enter your password',
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        child: Icon(_obscureText
                            ? Icons.visibility_off
                            : Icons.visibility),
                      ),
                    ),
                    validator: (val) {
                      if (val.length == 0) {
                        return "Please enter password";
                      } else if (val.length < 6) {
                        return "Password too short";
                      } else {
                        return null;
                      }
                    },
                    controller: _password,
                    obscureText: _obscureText,
                  ),
                  const SizedBox(height: 20),
                  RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        loginUser();
                      }
                    },
                    padding: const EdgeInsets.all(0.0),
                    textColor: Colors.white,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF0D47A1),
                            Color(0xFF42A5F5),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                      child: const Text('Log In',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20)),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        GestureDetector(
                          child: Text(
                            'Forget Password ?',
                            style: TextStyle(color: Colors.black),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgetPassPage()),
                            );
                          },
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap:(){
                          signInWithGoogle().whenComplete(() {
                          });
                        },
                        child: Container(
                          height: 70.0,
                          width: 70.0,
                          child: Image.asset('images/googlePlus.png'),
                        ),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      GestureDetector(
                        onTap: (){
                          initiateFacebookLogin();
                        },
                        child: Container(
                          height: 50.0,
                          width: 50.0,
                          child: Image.asset('images/facebook.png'),
                        ),
                      ),

                    ],
                  ),
                  const SizedBox(height: 20),
                  Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'don\'\t have an account? ',
                          style: TextStyle(color: Colors.grey),
                        ),
                        GestureDetector(
                          child: Text(
                            'Sign Up',
                            style: TextStyle(color: Colors.black),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignupPage()),
                            );
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
