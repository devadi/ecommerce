import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FeaturedPage extends StatefulWidget {
  List<dynamic> data = [];
  String text = "";

  FeaturedPage({this.data,this.text});

  @override
  FeaturedPageState createState() => FeaturedPageState(value: data,text:text);
}

class FeaturedPageState extends State<FeaturedPage> {
  List<dynamic> value = [];
  String text = "";

  FeaturedPageState({this.value,this.text});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                },
            );
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.black,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      body: new Container(
        padding: EdgeInsets.only(left: 15.0,right: 15.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                text,
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i) {
                    return new GestureDetector(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.only(bottom: 4.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            value == [''] || value[i]['product_image'] == null
                                ? Image.network(
                                    'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                                    height: 115,
                                    width: 130,
                                    fit: BoxFit.cover,
                                  )
                                : Image.network(
                                    'http://100.27.12.26:5000/resources/' +
                                        value[i]['product_image'],
                                    height: 115,
                                    width: 130,
                                    fit: BoxFit.cover,
                                  ),
                            SizedBox(
                              height: 4.0,
                            ),
                            Text('\$${value[i]['product_offerprice']}',
                              style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 4.0,
                            ),
                            Text('${value[i]['product_name']}',
                              style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
