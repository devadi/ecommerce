import 'package:ecommerce/login.dart';
import 'package:ecommerce/signup.dart';
import 'package:flutter/material.dart';

class SignupLoginPage extends StatefulWidget {
  @override
  SignupLoginPageState createState() => SignupLoginPageState();
}

class SignupLoginPageState extends State<SignupLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              SizedBox(height: 160.0,),
              Text('WELCOME TO INDORSE',
              style: TextStyle(color: Colors.grey,fontSize: 26.0),),
              SizedBox(height: 10.0,),
              Text('Explore Us',
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(height: 30.0,),
              Image(
                  image: AssetImage('images/signuplogin.jpg'),
              ),
              SizedBox(height: 10.0,),
              FlatButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => LoginPage()),);
                },
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                textColor: Colors.white,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    gradient: LinearGradient(
                      colors: <Color>[
                        Color(0xFF0D47A1),
                        Color(0xFF42A5F5),
                      ],
                    ),
                  ),
                  padding: const EdgeInsets.all(10.0),
                  child: const Text('Log in',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18)),
                ),
              ),
              FlatButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => SignupPage()),);
                },
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                textColor: Colors.grey,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(

                  ),
                  padding: const EdgeInsets.all(10.0),
                  child: const Text('Signup',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}