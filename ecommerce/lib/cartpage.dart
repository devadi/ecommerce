import 'dart:convert';

import 'package:ecommerce/checkoutpage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;
  var count;
  List<dynamic> value = [];
  List<int> price;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
          getCart();
        }
        else
        {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;

      }
    });
  }

  Future<Map<String, dynamic>> updateProductToCart(String count, String pid) async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/cart/addCartProduct";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          'product_id':pid,
          'Count':count,
          'uid':uid.toString()
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      price = [];
      for(int i =0;i<value.length;i++)
      {
        price.add(int.parse(value[i]['product_offerprice'])*int.parse(value[i]['counts']));
      }
      print(value);
      print(price);
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> getCart() async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/cart/getCart";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          'uid': uid.toString()
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      price = [];
      for(int i =0;i<value.length;i++)
        {
          price.add(int.parse(value[i]['product_offerprice'])*int.parse(value[i]['counts']));
        }
      print(value);
      print(price);
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }
  Future<Map<String, dynamic>> removeProduct(String cid) async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/cart/removeProduct";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          'uid':uid.toString(),
          "cartid":cid
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      price = [];
      for(int i =0;i<value.length;i++)
      {
        price.add(int.parse(value[i]['product_offerprice'])*int.parse(value[i]['counts']));
      }
      print(value);
      print(price);
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                "Cart",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                height: 200.0,
                child: new ListView.builder(
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i) {
                    return Container(
                      height: 130,
                      child: Card(
//                color: Colors.blue,
                        elevation: 10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: 100.0,
                                  height: 100.0,
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    image: DecorationImage(
                                        image: NetworkImage('http://100.27.12.26:5000/resources/' +
                                            value[i]['product_image']),
                                        fit: BoxFit.cover),
                                    /*borderRadius: BorderRadius.all(
                                          Radius.circular(75.0)),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 7.0,
                                            color: Colors.black)
                                      ]*/
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 8.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    '${value[i]['product_name']}',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 2.0),
                                  ),
                                  new Text(
                                    '${value[i]['product_description']}',
                                    style: TextStyle(
                                        fontSize: 14.0, color: Colors.grey),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 2.0),
                                  ),
                                  new Text(
                                    '\$${price[i].toString()}',
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Color(0xFF0D47A1),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Container(
                                    width: 140.0,
                                    height: 40.0,
                                    decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: (){
                                            count = int.parse('${value[i]['counts']}')-1;
                                            updateProductToCart(count.toString(),value[i]['product_id'].toString());
                                          },
                                          child: Icon(Icons.remove),
                                        ),
                                        SizedBox(
                                          width: 20.0,
                                        ),
                                        Container(
                                          child: Text('${value[i]['counts']}'),
                                        ),
                                        SizedBox(
                                          width: 20.0,
                                        ),
                                        GestureDetector(
                                          onTap: (){
                                            count = int.parse('${value[i]['counts']}')+1;
                                            updateProductToCart(count.toString(),value[i]['product_id'].toString());
                                          },
                                          child: Icon(Icons.add),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                removeProduct(value[i]['cartid'].toString());
                              },
                              child: Container(
                                alignment: Alignment.topRight,
                                child: Icon(
                                  Icons.close,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            value.length !=0
            ?FlatButton(
              color: Colors.white,
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => CheckOutPage(data:new Map(),flag:'cart')),);
              },
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              textColor: Colors.white,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xFF0D47A1),
                      Color(0xFF42A5F5),
                    ],
                  ),
                ),
                padding: const EdgeInsets.all(10.0),
                child: const Text('Continue',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18)),
              ),
            )
            :new Center(),
          ],
        ),
      ),
    );
  }
}
