import 'dart:convert';

import 'package:ecommerce/confirmationpage.dart';
import 'package:ecommerce/loadPaytmPage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class PaymentModePage extends StatefulWidget {
  String total;

  PaymentModePage({this.total});

  @override
  _PaymentModePage createState() => _PaymentModePage(grandTotal: total);
}

class _PaymentModePage extends State<PaymentModePage> {
  String grandTotal;
  int _radioValue = 0;
  String mode = "";
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;
  var email;
  var phone;
  Map data = new Map();
  Map res = new Map();
  var orderId;

  _PaymentModePage({this.grandTotal});

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          mode = 'cod';
          break;
        case 1:
          mode = 'paytm';
          break;
      }
    });
  }

  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          email = sharedPreferences.getString("email_ID");
          phone = sharedPreferences.getString("phone_no");
          print(uid);
        } else {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  Future<Map<String, dynamic>> checkout() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/Order/createOrder";
    print(myUrl);
    final response = await http.post(myUrl, headers: {
      'Accept': 'application/json'
    }, body: {
      'orderamount': grandTotal.toString(),
      'paymode': mode,
      'uid': uid.toString(),
      'address': '938, Road no 9, Shewrapara, Mirpur, Dhaka-1216'
    });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      data = parsedJson['data'];
      orderId = data['orderid'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      if (mode == 'cod') {
        Toast.show("" + parsedJson['message'], context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ConfirmPage()),
        );
      } else if(mode == 'paytm') {
        Toast.show("" + parsedJson['message'], context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoadPaytmView(id:orderId.toString(),amount:grandTotal,email:email,phone:phone)),
        );

      }
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> goToPaytm(var orderId) async {
    String myUrl = "http://100.27.12.26:5000/api/Paytm/payByPaytm?orderID=" +
        orderId.toString() +
        "&custID=" +
        "9871" +
        "&amount=" +
        grandTotal +
        "&custEmail=" +
        email +
        "&custPhone=" +
        phone;
    print(myUrl);
    final responses = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(responses.body);
    var parsedjson = json.decode(responses.body);
    print("Status = " + parsedjson['status']);
    res = parsedjson['data'];
    print(res);

//    updateTransaction();
  }

  Future<Map<String, dynamic>> updateTransaction() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                'Choose Payment Method',
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              child: new Card(
                elevation: 10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        'Cash On Delivery(COD)',
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: new Radio(
                            value: 0,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              child: new Card(
                elevation: 10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        'Paytm',
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: new Radio(
                            value: 1,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  onPressed: () {
                    checkout();
                  },
                  padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFF0D47A1),
                          Color(0xFF42A5F5),
                        ],
                      ),
                    ),
                    padding: const EdgeInsets.all(10.0),
                    child: const Text('Continue',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
