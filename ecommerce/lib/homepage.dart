import 'package:ecommerce/addresslist.dart';
import 'package:ecommerce/cartpage.dart';
import 'package:ecommerce/categorypage.dart';
import 'package:ecommerce/changepassword.dart';
import 'package:ecommerce/home.dart';
import 'package:ecommerce/marketpage.dart';
import 'package:ecommerce/orderpage.dart';
import 'package:ecommerce/profilepage.dart';
import 'package:ecommerce/wishlist.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

import 'orderhistory.dart';

class MyHomePage extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _cIndex = 0;
  final List<Widget> _children = [
    WishListPage(),
    HomePage(),
    MarketPage(),
    CartPage()
  ];

  void _incrementTab(index) {
    setState(() {
      _cIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.white,
          iconTheme: new IconThemeData(color: Colors.black),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.black,
              ),
              onPressed: () {
                // do something
              },
            )
          ],
        ),
        drawer: Drawer(

          child: ListView(
            children: [
              SizedBox(
                height: 50.0,
              ),
              ListTile(
                title: Text('Home'),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => MyHomePage()),);
                },
              ),
              ListTile(
                title: Text('Profile'),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ProfilePage()),);
                },
              ),
              ListTile(
                title: Text('Address'),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => AddressListPage()),);
                },
              ),
              ListTile(
                title: Text('My Orders'),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => OrderHistoryPage()),);
                },
              ),
              ListTile(
                title: Text('Change Password'),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ChangePassPage()),);
                },
              ),
            ],
          ),
        ),
        body: _children[_cIndex],
        bottomNavigationBar:BottomNavigationBar(

          currentIndex: _cIndex,
          selectedItemColor: Colors.blueAccent,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home,color: Color.fromARGB(255, 0, 0, 0)),
                title: new Text('Home',
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                )
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.category,color: Color.fromARGB(255, 0, 0, 0)),
                title: new Text('Categories',
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                )
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.shop,color: Color.fromARGB(255, 0, 0, 0)),
                title: new Text('Market',
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                )
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart,color: Color.fromARGB(255, 0, 0, 0)),
                title: new Text('Cart',
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                )
            )
          ],
          onTap: (index){
            _incrementTab(index);
          },
        )
    );
  }
}