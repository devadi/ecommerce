import 'dart:convert';
import 'package:ecommerce/paymentmode.dart';
import 'package:ecommerce/promocodelist.dart';
import 'package:http/http.dart' as http;
import 'package:ecommerce/confirmationpage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class CheckOutPage extends StatefulWidget {
  Map data;

  String flag;

  CheckOutPage({this.data, this.flag});

  @override
  CheckOutPageState createState() => CheckOutPageState(data: data, flag: flag);
}

class CheckOutPageState extends State<CheckOutPage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;
  List<dynamic> value = [];
  List<int> price;
  int totalPrice = 0;
  int grandTotal = 0;
  Map data;
  var discount = 0;

  String flag;

  CheckOutPageState({this.data, this.flag});

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
          getCart();
        } else {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  Future<Map<String, dynamic>> getCart() async {
    String myUrl = "$baseUrl/cart/getCart";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'}, body: {'uid': uid.toString()});
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      price = [];
      for (int i = 0; i < value.length; i++) {
        price.add(int.parse(value[i]['product_offerprice']) *
            int.parse(value[i]['counts']));
        totalPrice = totalPrice +
            (int.parse(value[i]['product_offerprice']) *
                int.parse(value[i]['counts']));
      }
      print(value);
      print(price);
      if (flag == 'promo') {
        if(data['type'] == 'flatRs')
          {
            discount = int.parse(data['amount']);
          }
        else
          {
            discount = ((5 / 100) * totalPrice).toInt();
          }
      } else {
        discount = 0;
      }
      grandTotal = totalPrice - discount + 10;
    });
    if (parsedJson['status'] == "1") {
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: new Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                "Checkout",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                height: 180.0,
                child: new ListView.builder(
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i) {
                    return Container(
                      height: 130,
                      child: Card(
//                color: Colors.blue,
                        elevation: 10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: 100.0,
                                  height: 100.0,
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            'http://100.27.12.26:5000/resources/' +
                                                value[i]['product_image']),
                                        fit: BoxFit.cover),
                                    /*borderRadius: BorderRadius.all(
                                          Radius.circular(75.0)),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 7.0,
                                            color: Colors.black)
                                      ]*/
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    '${value[i]['product_name']}',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 2.0),
                                  ),
                                  new Text(
                                    '${value[i]['product_description']}',
                                    style: TextStyle(
                                        fontSize: 14.0, color: Colors.grey),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 2.0),
                                  ),
                                  new Text(
                                    '\$${price[i].toString()}',
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Color(0xFF0D47A1),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  /*Container(
                                    width: 140.0,
                                    height: 40.0,
                                    decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Icon(Icons.remove),
                                        SizedBox(
                                          width: 20.0,
                                        ),
                                        Container(
                                          child: Text('1'),
                                        ),
                                        SizedBox(
                                          width: 20.0,
                                        ),
                                        Icon(Icons.add)
                                      ],
                                    ),
                                  ),*/
                                ],
                              ),
                            ),
                            /*Container(
                              alignment: Alignment.topRight,
                              child: Icon(
                                Icons.close,
                                color: Colors.grey,
                              ),
                            ),*/
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Shewrapara, Mirpur, Dhaka-1216',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    Text(
                      'House no : 938',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    Text(
                      'Road no : 9',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            flag == 'cart'
                ? new Container(
                    width: MediaQuery.of(context).size.width,
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'APPLY COUPON',
                          style: TextStyle(fontSize: 18.0),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              child: GestureDetector(
                                onTap: () {
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => PromoCodePage(),),);
                                },
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : new Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '${data['code']}',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Code applied on the bill',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.grey,
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    data = new Map();
                                    flag = 'cart';
                                    discount = 0;
                                    grandTotal = totalPrice - discount + 10;
                                  });
                                },
                                child: Icon(
                                  Icons.clear,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Subtotal',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18.0,
                    ),
                  ),
                  Text(
                    '\$${totalPrice.toString()}',
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            flag == 'promo'
                ? Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Discount',
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                          ),
                        ),
                        data['type'] == 'flatRs'
                        ?Text(
                          '-\$${data['amount']}',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        )
                        :Text(
                          '-5%',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ],
                    ),
                  )
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Shipping',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                    ),
                  ),
                  Text(
                    '\$10',
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 1.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Total',
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                  Text(
                    '\$${grandTotal.toString()}',
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            FlatButton(
              color: Colors.white,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PaymentModePage(total:grandTotal.toString())),
                );
              },
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              textColor: Colors.white,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xFF0D47A1),
                      Color(0xFF42A5F5),
                    ],
                  ),
                ),
                padding: const EdgeInsets.all(10.0),
                child: const Text('Continue',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18)),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
    );
  }
}
