import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class OrderHistoryPage extends StatefulWidget {
  @override
  OrderHistoryPageeState createState() => OrderHistoryPageeState();
}

class OrderHistoryPageeState extends State<OrderHistoryPage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;
  var count;
  List<dynamic> value = [];
  List<dynamic> data = [];
  List<dynamic> data2 = [];
  List<dynamic> data3 = [];
  List<int> price;
  List<int> price2;
  List<int> price3;
  List<String> status;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
          getOrderhistory();
        } else {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  Future<Map<String, dynamic>> getOrderhistory() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/Order/getOrderHistory";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'}, body: {'uid': uid.toString()});
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      price = [];
      price2 = [];
      price3 = [];
      status = [];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      for (int i = 0; i < value.length; i++) {
        if(value[i]['status'] == 'Order Placed')
          {
            for (int j = 0; j < value[i]['cartDetail'].length; j++) {
              for (int k = 0; k < value[i]['cartDetail'][j]['productDetail'].length; k++) {
                print(value[i]['cartDetail'][j]['productDetail']);
                data = value[i]['cartDetail'][j]['productDetail'];
                price.add(int.parse(value[i]['cartDetail'][j]['productDetail'][k]['product_offerprice']) * int.parse(value[i]['cartDetail'][j]['counts']));
              }
            }
          }
        else if(value[i]['status'] == 'Order Complete')
          {
            for (int j = 0; j < value[i]['cartDetail'].length; j++) {
              for (int k = 0; k < value[i]['cartDetail'][j]['productDetail'].length; k++) {
                print(value[i]['cartDetail'][j]['productDetail']);
                data2 = value[i]['cartDetail'][j]['productDetail'];
                price2.add(int.parse(value[i]['cartDetail'][j]['productDetail'][k]['product_offerprice']) * int.parse(value[i]['cartDetail'][j]['counts']));
              }
            }
          }
        else{
          for (int j = 0; j < value[i]['cartDetail'].length; j++) {
            for (int k = 0; k < value[i]['cartDetail'][j]['productDetail'].length; k++) {
              print(value[i]['cartDetail'][j]['productDetail']);
              data3 = value[i]['cartDetail'][j]['productDetail'];
              price3.add(int.parse(value[i]['cartDetail'][j]['productDetail'][k]['product_offerprice']) * int.parse(value[i]['cartDetail'][j]['counts']));
            }
          }
        }
      }
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    Widget tab1 = new Container(
      child: new Expanded(
        child: new Container(
          height: 200.0,
          child: new ListView.builder(
            itemCount: data.length,
            itemBuilder: (BuildContext context, int i) {
              return Container(
                height: 130,
                child: Card(
//                color: Colors.blue,
                  elevation: 10,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: GestureDetector(
                          onTap: () {},
                          child: Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'http://100.27.12.26:5000/resources/' +
                                          data[i]['product_image']),
                                  fit: BoxFit.cover),
                              /*borderRadius: BorderRadius.all(
                                          Radius.circular(75.0)),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 7.0,
                                            color: Colors.black)
                                      ]*/
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                '${data[i]['product_name']}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Text(
                                '${data[i]['product_description']}',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.grey),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Text(
                                '\$${price[i].toString()}',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Color(0xFF0D47A1),
                                ),
                              ),
                              FlatButton(
                                color: Colors.white,
                                onPressed: () {},
                                padding: const EdgeInsets.all(0.0),
                                textColor: Colors.white,
                                child: Container(
                                  decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                    gradient: LinearGradient(
                                      colors: <Color>[
                                        Color(0xFF0D47A1),
                                        Color(0xFF42A5F5),
                                      ],
                                    ),
                                  ),
                                  padding: const EdgeInsets.all(10.0),
                                  child: const Text('Cancel Order',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
    Widget tab2 = new Container(
      child: new Expanded(
        child: new Container(
          height: 200.0,
          child: new ListView.builder(
            itemCount: data2.length,
            itemBuilder: (BuildContext context, int i) {
              return Container(
                height: 130,
                child: Card(
//                color: Colors.blue,
                  elevation: 10,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: GestureDetector(
                          onTap: () {},
                          child: Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'http://100.27.12.26:5000/resources/' +
                                          data2[i]['product_image']),
                                  fit: BoxFit.cover),
                              /*borderRadius: BorderRadius.all(
                                          Radius.circular(75.0)),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 7.0,
                                            color: Colors.black)
                                      ]*/
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                '${data2[i]['product_name']}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Text(
                                '${data2[i]['product_description']}',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.grey),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Text(
                                '\$${price2[i].toString()}',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Color(0xFF0D47A1),
                                ),
                              ),
                              FlatButton(
                                color: Colors.white,
                                onPressed: () {},
                                padding: const EdgeInsets.all(0.0),
                                textColor: Colors.white,
                                child: Container(
                                  decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                    gradient: LinearGradient(
                                      colors: <Color>[
                                        Color(0xFF0D47A1),
                                        Color(0xFF42A5F5),
                                      ],
                                    ),
                                  ),
                                  padding: const EdgeInsets.all(10.0),
                                  child: const Text('Order Again',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
    Widget tab3 = new Container(
      child: new Expanded(
        child: new Container(
          height: 200.0,
          child: new ListView.builder(
            itemCount: data3.length,
            itemBuilder: (BuildContext context, int i) {
              return Container(
                height: 130,
                child: Card(
//                color: Colors.blue,
                  elevation: 10,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: GestureDetector(
                          onTap: () {},
                          child: Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'http://100.27.12.26:5000/resources/' +
                                          data3[i]['product_image']),
                                  fit: BoxFit.cover),
                              /*borderRadius: BorderRadius.all(
                                          Radius.circular(75.0)),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 7.0,
                                            color: Colors.black)
                                      ]*/
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                '${data3[i]['product_name']}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Text(
                                '${data3[i]['product_description']}',
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.grey),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Text(
                                '\$${price3[i].toString()}',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Color(0xFF0D47A1),
                                ),
                              ),
                              FlatButton(
                                color: Colors.white,
                                onPressed: () {},
                                padding: const EdgeInsets.all(0.0),
                                textColor: Colors.white,
                                child: Container(
                                  decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                    gradient: LinearGradient(
                                      colors: <Color>[
                                        Color(0xFF0D47A1),
                                        Color(0xFF42A5F5),
                                      ],
                                    ),
                                  ),
                                  padding: const EdgeInsets.all(10.0),
                                  child: const Text('Order Again',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.white,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.grey,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            },
          ),
          bottom: TabBar(
            labelColor: Color(0xFF0D47A1),
            tabs: [
              Tab(
                text: 'Placed Order',
              ),
              Tab(
                text: 'Complete Order',
              ),
              Tab(
                text: 'Cancel Order',
              ),
            ],
          ),
        ),
        body: new TabBarView(
          children: <Widget>[
            tab1,
            tab2,
            tab3,
          ],
        ),
      ),
    );
  }
}
