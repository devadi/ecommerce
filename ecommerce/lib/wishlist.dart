import 'dart:convert';

import 'package:ecommerce/featuredpage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class WishListPage extends StatefulWidget {
  @override
  WishListPageState createState() => WishListPageState();
}

class WishListPageState extends State<WishListPage> {
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];
  List<dynamic> randomValue = [];
  List<dynamic> latestValue = [];
  List<dynamic> bestSellValue = [];
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
          getrandomProductList();
        } else {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  Future<Map<String, dynamic>> getrandomProductList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/product/randomProductList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      randomValue = parsedJson['data'];
    });
    getlatestProductList();
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> getlatestProductList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/product/latestProductList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      latestValue = parsedJson['data'];
    });
    getbestSellProductList();
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> getbestSellProductList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/product/getbestSellProductList";
    print(myUrl);
    final response = await http.get(
      myUrl,
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      bestSellValue = parsedJson['data'];
    });
    getWishList();
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }




  Future<Map<String, dynamic>> getWishList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/wishlist/wishlistProducts";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'}, body: {'uid': uid.toString()});
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      print(value);
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    Widget horizontalList1 = new Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 195.0,
      child: new Expanded(
        child: new Container(
          child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: value.length,
            itemBuilder: (BuildContext context, int i) {
              return new Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    value == [''] || value[i]['product_image'] == null
                        ? Image.network(
                            'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            'http://100.27.12.26:5000/resources/' +
                                value[i]['product_image'],
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          ),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('\$${value[i]['product_offerprice']}'),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('${value[i]['product_name']}'),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
    Widget horizontalList3 = new Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 195.0,
      child: new Expanded(
        child: new Container(
          child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: randomValue.length,
            itemBuilder: (BuildContext context, int i) {
              return new Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    randomValue == [''] ||
                            randomValue[i]['product_image'] == null
                        ? Image.network(
                            'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            'http://100.27.12.26:5000/resources/' +
                                randomValue[i]['product_image'],
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          ),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('\$${randomValue[i]['product_offerprice']}'),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('${randomValue[i]['product_name']}'),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
    Widget horizontalList4 = new Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 195.0,
      child: new Expanded(
        child: new Container(
          child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: latestValue.length,
            itemBuilder: (BuildContext context, int i) {
              return new Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    latestValue == [''] ||
                            latestValue[i]['product_image'] == null
                        ? Image.network(
                            'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            'http://100.27.12.26:5000/resources/' +
                                latestValue[i]['product_image'],
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          ),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('\$${latestValue[i]['product_offerprice']}'),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('${latestValue[i]['product_name']}'),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
    Widget horizontalList2 = new Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 195.0,
      child: new Expanded(
        child: new Container(
          child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: bestSellValue.length,
            itemBuilder: (BuildContext context, int i) {
              return new Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    bestSellValue == [''] ||
                            bestSellValue[i]['product_image'] == null
                        ? Image.network(
                            'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            'http://100.27.12.26:5000/resources/' +
                                bestSellValue[i]['product_image'],
                            height: 125,
                            width: 130,
                            fit: BoxFit.cover,
                          ),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('\$${bestSellValue[i]['product_offerprice']}'),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text('${bestSellValue[i]['product_name']}'),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
    return Scaffold(
      body: new Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Expanded(
          child: new ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              SizedBox(
                height: 120.0,
                width: MediaQuery.of(context).size.width,
                child: Carousel(
                  images: [
                    NetworkImage(
                        'http://www.newperuvian.com/wp-content/uploads/2018/08/uno-clothes-shopping-in-lima.jpg'),
                    NetworkImage(
                        'https://thumbs.dreamstime.com/z/shopping-men-s-department-clothing-featuring-popular-brands-clothing-bargain-store-marshall-fashion-retail-30915521.jpg'),
                    NetworkImage(
                        'https://cdn.dnaindia.com/sites/default/files/styles/full/public/2019/02/17/792362-mumbai-mall-dna.jpg'),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Featured',
                        style: new TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FeaturedPage(
                                      data: randomValue, text: 'Featured'),
                                ),
                              );
                            },
                            child: Text(
                              'See all',
                              style: new TextStyle(
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              horizontalList3,
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Latest',
                        style: new TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FeaturedPage(
                                      data: latestValue, text: 'Latest'),
                                ),
                              );
                            },
                            child: Text(
                              'See all',
                              style: new TextStyle(
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              horizontalList4,
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Best Sell',
                        style: new TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: Text(
                            'See all',
                            style: new TextStyle(
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              horizontalList2,
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'WishList',
                        style: new TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FeaturedPage(
                                      data: value, text: 'WishList'),
                                ),
                              );
                            },
                            child: Text(
                              'See all',
                              style: new TextStyle(
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              horizontalList1,
            ],
          ),
        ),
      ),
    );
  }
}
