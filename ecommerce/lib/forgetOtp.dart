import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import 'login.dart';

class ForgetOtpScreen extends StatefulWidget {
  String otp;
  String uid;
  String email;
  String password;
  ForgetOtpScreen({this.otp,this.email,this.password});

  @override
  _ForgetOtpScreen createState() => _ForgetOtpScreen(otp:otp,email:email,password:password);
}

class _ForgetOtpScreen extends State<ForgetOtpScreen> {


  String baseUrl = "http://100.27.12.26:5000/api";
  SharedPreferences sharedPreferences;
  String otp;
  String email;
  String password;
  String uid;
  Timer _timer;
  int _start = 30;
  _ForgetOtpScreen({this.otp,this.email,this.password});
  @override
  void initState() {
    super.initState();
    getOtp();
    startTimer();
  }
  @override
  ProgressDialog pr;
  Map<String,dynamic> value;
  final _formkey = GlobalKey<FormState>();

  final controller1 = TextEditingController();
  final controller2 = TextEditingController();
  final controller3 = TextEditingController();
  final controller4 = TextEditingController();

  FocusNode textFirstFocusNode = new FocusNode();
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textThirdFocusNode = new FocusNode();
  FocusNode textFourthFocusNode = new FocusNode();

  Future<Map<String, dynamic>> getData(String s) async {
    print(email);
    print(password);
    print(controller1.text);
    print(controller2.text);
    print(controller3.text);
    print(controller4.text);
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    // print(token);
    String myUrl = "$baseUrl/User/forgetPassword";
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body: {
          "Email": email,
          "Password":password,
          "otp": controller1.text+
              controller2.text+
              controller3.text+
              controller4.text,
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    setState(() {
      value = parsedJson['data'];
    });
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      _onChanged(mail);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _formkey,
        body: GestureDetector(
          onTap: () {

            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[
//              Container(
//                  alignment: Alignment.center,
//                  padding: EdgeInsets.only(bottom: 15.0),
//                  child: Text(
//                    'Confirm Number',
//                    style: TextStyle(
//                        color: Colors.white,
//                        fontWeight: FontWeight.w500,
//                        fontFamily: "Roboto",
//                        fontSize: 20),
//                  )),
                    SizedBox(height: 120.0),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'We sent a Sign Up code',
                          style: TextStyle(fontSize: 30),
                        )),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Enter the 4-digit code sent to your',
                          style: TextStyle(fontSize: 18),
                        )),
                    Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 10,right: 10.0),
                        child: Text(
                          'Mobile No',
                          style: TextStyle(fontSize: 18),
                        )),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      margin: new EdgeInsets.fromLTRB(30.0, 30.0, 30.0,  25.0),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: <Widget>[
                                Flexible(
                                  child: Container(
                                    //margin: EdgeInsets.fromLTRB(15,0,10,0),
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 2.0, left: 2.0),
                                      child: new Container(
                                          alignment: Alignment.center,
                                          decoration: new BoxDecoration(
                                              color: Color.fromRGBO(0, 0, 0, 0.3),
                                              border: new Border.all(
                                                  width: 1.0,
                                                  color:
                                                  Color.fromRGBO(0, 0, 0, 0.1)),
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(100.0))),
                                          child: new TextFormField(
                                            decoration: InputDecoration(
                                                border: InputBorder.none),
                                            keyboardType: TextInputType.phone,
                                            textInputAction: TextInputAction.next,
                                            onFieldSubmitted: (String value) {
                                              FocusScope.of(context).requestFocus(textSecondFocusNode);
                                            },
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  1),
                                            ],
                                            enabled: true,
                                            controller: controller1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24.0, color: Colors.black),
                                          )),
                                    ),
//                                PinInputTextField(
//                                  pinLength: 4,
//                                  decoration: UnderlineDecoration(
//                                      textStyle: TextStyle(
//                                          color: Colors.black)),
//                                ),
                                  ),
                                ),
                                Flexible(
                                  child: Container(
                                    // margin: EdgeInsets.fromLTRB(10,0,10,0),
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 2.0, left: 2.0),
                                      child: new Container(
                                          alignment: Alignment.center,
                                          decoration: new BoxDecoration(
                                              color: Color.fromRGBO(0, 0, 0, 0.3),
                                              border: new Border.all(
                                                  width: 1.0,
                                                  color:
                                                  Color.fromRGBO(0, 0, 0, 0.1)),
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(100.0))),
                                          child: new TextFormField(
//                                      maxLength: 1,
                                            decoration: InputDecoration(
                                                border: InputBorder.none),
                                            keyboardType: TextInputType.phone,
                                            focusNode: textSecondFocusNode,
                                            textInputAction: TextInputAction.next,
                                            onFieldSubmitted: (String value) {
                                              FocusScope.of(context).requestFocus(textThirdFocusNode);
                                            },
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  1),
                                            ],
                                            enabled: true,
                                            controller: controller2,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24.0, color: Colors.black),
                                          )),
                                    ),
//                                PinInputTextField(
//                                  pinLength: 4,
//                                  decoration: UnderlineDecoration(
//                                      textStyle: TextStyle(
//                                          color: Colors.black)),
//                                ),
                                  ),
                                ),
                                Flexible(
                                  child: Container(
                                    // margin: EdgeInsets.fromLTRB(10,0,10,0),
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 2.0, left: 2.0),
                                      child: new Container(
                                          alignment: Alignment.center,
                                          decoration: new BoxDecoration(
                                            color: Color.fromRGBO(0, 0, 0, 0.3),
                                            border: new Border.all(
                                                width: 1.0,
                                                color: Color.fromRGBO(0, 0, 0, 0.1)),
                                            borderRadius: new BorderRadius.all(
                                                Radius.circular(200.0)),
                                          ),
                                          child: new TextFormField(
//                                      maxLength: 1,
                                            decoration: InputDecoration(
                                                border: InputBorder.none),
                                            keyboardType: TextInputType.phone,
//                                            focusNode: textThirdFocusNode,
                                            textInputAction: TextInputAction.next,
                                            onFieldSubmitted: (value) {
                                              FocusScope.of(context).requestFocus(textFourthFocusNode);
                                            },
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  1),
                                            ],
                                            enabled: true,
                                            controller: controller3,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24.0, color: Colors.black),
                                          )),
                                    ),
//                                PinInputTextField(
//                                  pinLength: 4,
//                                  decoration: UnderlineDecoration(
//                                      textStyle: TextStyle(
//                                          color: Colors.black)),
//                                ),
                                  ),
                                ),
                                Flexible(
                                  child: Container(
                                    //  margin: EdgeInsets.fromLTRB(10,0,15,0),
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 2.0, left: 2.0),
                                      child: new Container(
                                          alignment: Alignment.center,
                                          decoration: new BoxDecoration(
                                              color: Color.fromRGBO(0, 0, 0, 0.3),
                                              border: new Border.all(
                                                  width: 1.0,
                                                  color:
                                                  Color.fromRGBO(0, 0, 0, 0.1)),
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(100.0))),
                                          child: new TextFormField(
//                                    maxLength: 1,
                                            decoration: InputDecoration(
                                                border: InputBorder.none),
                                            keyboardType: TextInputType.phone,
                                            focusNode: textFourthFocusNode,
                                            textInputAction: TextInputAction.done,
//                                      onFieldSubmitted: (value) {
//                                        FocusScope.of(context).requestFocus(textFourthFocusNode);
//                                      },
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  1),
                                            ],
                                            enabled: true,
                                            controller: controller4,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24.0, color: Colors.black),
                                          )),
                                    ),
//                                PinInputTextField(
//                                  pinLength: 4,
//                                  decoration: UnderlineDecoration(
//                                      textStyle: TextStyle(
//                                          color: Colors.black)),
//                                ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        getData(controller1.text+controller2.text+controller3.text+controller4.text);
                      },
                      color: Colors.white,
                      padding: const EdgeInsets.only(left: 10.0,right: 10.0),
                      textColor: Colors.white,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color(0xFF0D47A1),
                              Color(0xFF42A5F5),
                            ],
                          ),
                        ),
                        padding: const EdgeInsets.all(10.0),
                        child: const Text('Submit',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 20)),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        ('Re-send Code in ')+_start.toString()+" "+('seconds'),
                        style: TextStyle(
                          fontFamily: "Roboto",
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));
  }
  void startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {

      setState(() {
        _start--;
        print(_start);
      });
      if(_start == -1)
      {
//        reSendOtp();
        _start = 30;
      }
      if(_start == 25)
      {
        getOtp();
      }

    });
  }

  getOtp(){
    List<String> o = otp.split('');
    print("o is-- $o");
    controller1.text = o[0];
    controller2.text = o[1];
    controller3.text = o[2];
    controller4.text = o[3];
  }

  _onChanged(String text) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setBool("check", true);
//      sharedPreferences.setString("mail", mail);
      sharedPreferences.commit();
      print("s"+text);
    });
  }
}
