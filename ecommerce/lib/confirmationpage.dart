import 'package:ecommerce/home.dart';
import 'package:ecommerce/homepage.dart';
import 'package:flutter/material.dart';

class ConfirmPage extends StatefulWidget {
  @override
  _ConfirmPageState createState() => _ConfirmPageState();
}

class _ConfirmPageState extends State<ConfirmPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'images/confirm.png',
            ),
            Text(
              'Confirmation',
              style: TextStyle(fontSize: 26.0),
            ),
            SizedBox(height: 5.0,),
            Text(
              'You have successfully\ncompleted your payment procedure',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14.0,
              color: Colors.grey),
            ),
            SizedBox(height: 50.0,),
            FlatButton(
              color: Colors.white,
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => MyHomePage()),);
              },
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              textColor: Colors.white,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0)),
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xFF0D47A1),
                      Color(0xFF42A5F5),
                    ],
                  ),
                ),
                padding: const EdgeInsets.all(10.0),
                child: const Text('Next',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
