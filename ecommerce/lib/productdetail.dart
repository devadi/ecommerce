import 'dart:convert';

import 'package:ecommerce/cartpage.dart';
import 'package:ecommerce/homepage.dart';
import 'package:ecommerce/shopdetail.dart';
import 'package:ecommerce/wishlist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class ProductDetailPage extends StatefulWidget {
  Map data;

  ProductDetailPage({this.data});

  @override
  ProductDetailPageState createState() => ProductDetailPageState(value: data);
}

class ProductDetailPageState extends State<ProductDetailPage> {
  Map value;

  ProductDetailPageState({this.value});

  var uid;
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  List<dynamic> data = [];
  String cartId = "";
  String wid = "";
  bool _isVisible = true;
  bool _isVisible1 = false;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  void showSize() {
    setState(() {
      _isVisible = true;
      _isVisible1 = false;
    });
  }

  void showColor() {
    setState(() {
      _isVisible1 = true;
      _isVisible = false;
    });
  }

  Future<Map<String, dynamic>> getCount() async {
    String myUrl = "$baseUrl/cart/getCount";
    print(myUrl);
    final response = await http.post(myUrl, headers: {
      'Accept': 'application/json'
    }, body: {
      'product_id': value['product_id'].toString(),
      'uid': uid.toString()
    });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      cartId = parsedJson['data']['cartid'].toString();
      print(cartId);
      print('jccggmgj');
    });
    if (parsedJson['status'] == "1") {
      cartId = parsedJson['data']['cartid'].toString();
      getProWishlist();
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      cartId = "";
      getProWishlist();
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> getProWishlist() async {
    String myUrl = "$baseUrl/wishlist/getProWishlist";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body: {'pid': value['product_id'].toString(), 'uid': uid.toString()});
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      wid = parsedJson['data']['wid'].toString();
      print(wid);
      print('jgfvgvcgn');
    });
    if (parsedJson['status'] == "1") {
      wid = parsedJson['data']['wid'].toString();
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      wid = "";
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> addProductToCart() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/cart/addCartProduct";
    print(myUrl);
    final response = await http.post(myUrl, headers: {
      'Accept': 'application/json'
    }, body: {
      'product_id': value['product_id'].toString(),
      'Count': '1',
      'uid': uid.toString()
    });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      getCount();
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  Future<Map<String, dynamic>> addProductToWishList() async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/wishlist/addWishList";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body: {'pid': value['product_id'].toString(), 'uid': uid.toString()});
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      getCount();
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
          getCount();
        } else {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: new Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.network(
              'http://100.27.12.26:5000/resources/' + value['product_image'],
              height: 150.0,
              width: MediaQuery.of(context).size.width,
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              '${value['product_name']}',
              style: TextStyle(fontSize: 20.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '\$${value['product_offerprice']}',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Color(0xFF0D47A1),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    '\$${value['product_price']}',
                    style: TextStyle(
                        fontSize: 12.0, decoration: TextDecoration.lineThrough),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  (wid == "")
                      ? Align(
                          alignment: Alignment.topRight,
                          child: GestureDetector(
                            onTap: () {
                              addProductToWishList();
                            },
                            child: Text(
                              'Add To WishList',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Color(0xFF0D47A1),
                                  decoration: TextDecoration.underline),
                            ),
                          ),
                        )
                      : Align(
                          alignment: Alignment.topRight,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => WishListPage()),
                              );
                            },
                            child: Text(
                              'View WishList',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Color(0xFF0D47A1),
                                  decoration: TextDecoration.underline),
                            ),
                          ),
                        ),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 1.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 40,
                  width: 40,
                  color: Color(0xFF0D47A1),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      '4.5',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                Text(
                  'Very Good',
                  style: TextStyle(fontSize: 20.0),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Text(
                    '49 Reviews',
                    style: TextStyle(
                      fontSize: 14.0,
                      color: Color(0xFF0D47A1),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 1.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              '${value['product_description']}',
              style: TextStyle(fontSize: 14.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'Shop',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                (value['shop_name'] == null)
                    ? SizedBox(
                        width: 0.0,
                        height: 0.0,
                      )
                    : GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ShopDetailPage(
                                    name: value['shop_name'],
                                    image: value['shop_image'],
                                    address: value['shop_address'],
                                    phone: value['shop_phoneNo'])),
                          );
                        },
                        child: Text(
                          '${value['shop_name']}',
                          style: TextStyle(fontSize: 14.0),
                        ),
                      ),
                Text(
                  value['shop_address'] == null
                      ? ''
                      : '${value['shop_address']}',
                  style: TextStyle(fontSize: 14.0),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 1.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    showSize();
                  },
                  child: Text(
                    'Select Size',
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    showColor();
                  },
                  child: Text(
                    'Select Color',
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 1.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            SizedBox(
              height: 10.0,
            ),
            Visibility(
                visible: _isVisible,
                child: Center(
                  child: Container(
                    child: Text(
                      '${value['product_availableSize']}',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                )),
            Visibility(
                visible: _isVisible1,
                child: Center(
                  child: Container(
                    child: Text(
                      '${value['product_availableColor']}',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                )),
            (cartId == "")
                ? Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: RaisedButton(
                        onPressed: () {
                          addProductToCart();
                        },
                        padding: const EdgeInsets.all(0.0),
                        textColor: Colors.white,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF0D47A1),
                                Color(0xFF42A5F5),
                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: const Text('ADD TO CART',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20)),
                        ),
                      ),
                    ),
                  )
                : Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => CartPage()),
                          );
                        },
                        padding: const EdgeInsets.all(0.0),
                        textColor: Colors.white,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF0D47A1),
                                Color(0xFF42A5F5),
                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: const Text('GO TO CART',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20)),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
