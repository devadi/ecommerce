import 'dart:convert';

import 'package:ecommerce/createaddresspage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class AddressListPage extends StatefulWidget {
  @override
  AddressListPageState createState() => AddressListPageState();
}

class AddressListPageState extends State<AddressListPage> {

  int _radioValue = 0;
  double _result = 0.0;

  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  SharedPreferences sharedPreferences;
  bool checkValue;
  var uid;
  var count;
  List<dynamic> value = [];
  List<int> price;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      print(checkValue);
      if (checkValue != null) {
        if (checkValue) {
          uid = sharedPreferences.getString("userid");
          print(uid);
          getAddressList();
        }
        else
        {
          uid.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;

      }
    });
  }

  Future<Map<String, dynamic>> getAddressList() async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/Address/getAddressList";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body:
        {
          'uid': uid.toString()
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);

    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: new Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                "Address",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                child: new ListView.builder(
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i) {
                    return Container(
                      padding: EdgeInsets.only(top: 25.0),
                      width: MediaQuery.of(context).size.width,
                      child: new GestureDetector(
                        onTap: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(0.0),
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${value[i]['addressLane']}',
                                      style: TextStyle(
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    Text(
                                      'House no : ${value[i]['houseNo']}',
                                      style: TextStyle(
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    Text(
                                      'Road no : ${value[i]['roadNo']}',
                                      style: TextStyle(
                                        fontSize: 18.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  child: new Radio(
                                    value: i,
                                    groupValue: _radioValue,
                                    onChanged: _handleRadioValueChange,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            FlatButton(
              color: Colors.white,
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => NewAddressPage()),);
              },
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              textColor: Color(0xFF0D47A1),
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.5,
                    color: Colors.black,
                  ),
                ),
                padding: const EdgeInsets.all(10.0),
                child: const Text('Add Address',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18)),
              ),
            ),
            FlatButton(
              color: Colors.white,
              onPressed: () {},
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              textColor: Colors.white,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xFF0D47A1),
                      Color(0xFF42A5F5),
                    ],
                  ),
                ),
                padding: const EdgeInsets.all(10.0),
                child: const Text('Next',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _result = 1.0;
          break;
        case 1:
          _result = 2.0;
          break;
        case 2:
          _result = 3.0;
          break;
      }
    });
  }
}
