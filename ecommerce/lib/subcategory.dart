import 'dart:convert';
import 'package:ecommerce/productbycat.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';

class SubCategoryPage extends StatefulWidget {
  Map data;

  SubCategoryPage({this.data});

  @override
  SubCategoryPageState createState() => SubCategoryPageState(data: data);
}

class SubCategoryPageState extends State<SubCategoryPage> {
  Map data;

  SubCategoryPageState({this.data});
  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];


  Future<Map<String, dynamic>> getSubCategoryList() async {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/SubCategory/subCategoryList";
    print(myUrl);
    final response = await http.post(myUrl,
      headers: {'Accept': 'application/json'},
      body:{'cid': data['cid'].toString()}
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {

      value = parsedJson['data'];
    });
    print(value);
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  void initState() {
    super.initState();

    getSubCategoryList();
  }

  @override
  Widget build(BuildContext context) {
    print(data['C_Name']);
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                },
            );
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                "Sub Categories",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                height: 200.0,
                child: new ListView.builder(
                    itemCount: value.length,
                    itemBuilder: (BuildContext context, int i) {
                      return new GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                            MaterialPageRoute(builder: (context) => CatProductPage(data:value[i])),
                          );
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 80,
                          child: Card(
                            elevation: 10,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(left: 10.0,right: 10.0,top: 15.0),
                                  child: Text('${value[i]['SC_Name']}',
                                  style: TextStyle(fontSize: 16.0),
                                  ),
                                ),Padding(
                                    padding: EdgeInsets.only(left: 10.0,right: 10.0,top: 10.0),
                                  child: Text('${value[i]['SC_Description']}',
                                  style: TextStyle(fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
