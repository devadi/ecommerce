import 'package:ecommerce/home.dart';
import 'package:ecommerce/homepage.dart';
import 'package:ecommerce/login.dart';
import 'package:ecommerce/signuploginpage.dart';
import 'package:ecommerce/splashpage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashPage(),
      routes: <String, WidgetBuilder>{
        '/SignupLoginScreen': (BuildContext context) => new SignupLoginPage(),
        '/Home': (BuildContext context) => new MyHomePage(),
        '/Login': (BuildContext context) => new LoginPage()
      },
//      home: FavouritePage(),
    );
  }
}


