import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class ShopDetailPage extends StatefulWidget {
  String name,image,address,phone;

  ShopDetailPage({this.name,this.image,this.address,this.phone});

  @override
  ShopDetailPageState createState() => ShopDetailPageState(name:name,image:image,address:address,phone:phone);
}

class ShopDetailPageState extends State<ShopDetailPage> {

  ShopDetailPageState({this.name,this.image,this.address,this.phone});

  String name,image,address,phone;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                },
            );
          },
        ),
      ),
      body: new Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.network(
              'http://100.27.12.26:5000/resources/' + image,
              height: 150.0,
              width: MediaQuery.of(context).size.width,
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              name,
              style: TextStyle(fontSize: 20.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 1.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'Address',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              address,
              style: TextStyle(fontSize: 16.0,
              color: Color(0xFF0D47A1),
              ),
            ),

            SizedBox(
              height: 10.0,
            ),

            Text(
              'Shop Phone Number',
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              phone,
              style: TextStyle(fontSize: 16.0,
              color: Color(0xFF0D47A1),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
