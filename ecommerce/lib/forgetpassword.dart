import 'package:ecommerce/forgetOtp.dart';
import 'package:ecommerce/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ForgetPassPage extends StatefulWidget {

  @override
  ForgetPassPageState createState() => ForgetPassPageState();
}

class ForgetPassPageState extends State<ForgetPassPage> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String baseUrl = "http://100.27.12.26:5000/api";
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _cpassword = TextEditingController();
  bool _obscureText = true;
  bool _obscureText1 = true;
  ProgressDialog pr;
  Map<String,dynamic> value;
  String otp,email,password;

  Future<Map<String, dynamic>> forgetPassword() async {

    print(_email.text);
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal,isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/User/forgetPasswordGetOTP";
    print(myUrl);
    final response = await http.post(myUrl,
        headers: {'Accept': 'application/json'},
        body: {"Email": _email.text
        });
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
      otp = value['otp'];
      email = _email.text;
      password = _password.text;
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      _email.clear();
      _password.clear();
      _cpassword.clear();
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
      Navigator.pop(context);
      Navigator.push(context,
        MaterialPageRoute(builder: (context) => ForgetOtpScreen(otp:otp,email:email,password:password)),
      );
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
            builder: (BuildContext context){
              return IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.grey,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            }),
      ),
      body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.only(left: 15.0,right: 15.0),
              child: new Column(
                children: <Widget>[
                  SizedBox(
                    height: 35.0,
                  ),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text("Forgot Password",
                      style: new TextStyle(
                          fontSize: 26.0
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),

                  TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                        labelText: 'Email/Number',
                        hintText: 'Enter your Email Id/Phone Number'
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (val){
                      if(val.length == 0)
                      {
                        return "Please enter email/phone Number";
                      }
                      else
                      {
                        return null;
                      }
                    },
                    controller: _email,
                  ),
                  const SizedBox(height: 20),

                  TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                      labelText: 'New Password',
                      hintText: 'Enter your password',
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        child: Icon(_obscureText ? Icons.visibility_off : Icons.visibility),
                      ),
                    ),
                    validator: (val)
                    {
                      if(val.length == 0)
                      {
                        return "Please enter password";
                      }
                      else if(val.length < 6)
                      {
                        return "Password too short";
                      }
                      else
                      {
                        return null;
                      }
                    },
                    controller: _password,
                    obscureText: _obscureText,
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                      labelText: 'Confirm Password',
                      hintText: 'Re Enter your password',
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText1 = !_obscureText1;
                          });
                        },
                        child: Icon(_obscureText1 ? Icons.visibility_off : Icons.visibility),
                      ),
                    ),
                    validator: (val)
                    {
                      if(val.length == 0)
                      {
                        return "Please enter password";
                      }
                      else if(val!=_password.text)
                      {
                        return "Password not matched";
                      }
                      else
                      {
                        return null;
                      }
                    },
                    controller: _cpassword,
                    obscureText: _obscureText1,
                  ),
                  const SizedBox(height: 20),
                  RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        forgetPassword();
                      }
                    },
                    padding: const EdgeInsets.all(0.0),
                    textColor: Colors.white,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF0D47A1),
                            Color(0xFF42A5F5),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                      child: const Text(
                          'Continue',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20)
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
      ),
    );
  }
}
