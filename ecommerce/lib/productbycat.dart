import 'dart:convert';

import 'package:ecommerce/productdetail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class CatProductPage extends StatefulWidget {
  Map data;

  CatProductPage({this.data});
  @override
  CatProductPageState createState() => CatProductPageState(result:data);
}

class CatProductPageState extends State<CatProductPage> {
  Map result;

  CatProductPageState({this.result});

  String baseUrl = "http://100.27.12.26:5000/api";
  ProgressDialog pr;
  List<dynamic> value = [];


  @override
  void initState() {
    super.initState();
    getProductListByCid(result['cid']);
  }

  Future<Map<String, dynamic>> getProductListByCid(String cid) async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    pr.show();
    String myUrl = "$baseUrl/product/getProductByCategory";
    print(myUrl);
    final response = await http.post(
      myUrl,
      headers: {'Accept': 'application/json'},
      body: {
        'catId':cid
      },
    );
    print(response.body);
    var parsedJson = json.decode(response.body);
    print("Status = " + parsedJson['status']);
    setState(() {
      value = parsedJson['data'];
    });
    if (parsedJson['status'] == "1") {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      Navigator.of(context).pushNamedAndRemoveUntil('/screen1', (Route<dynamic> route) => false);
    } else {
      pr.hide();
      Toast.show("" + parsedJson['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    return parsedJson;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.black,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      body: new Container(
        padding: EdgeInsets.only(left: 30.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            new Container(
              height: 50.0,
              alignment: Alignment.topLeft,
              child: new Text(
                "Products",
                style: new TextStyle(fontSize: 26.0),
              ),
            ),
            new Expanded(
              child: new Container(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                  itemCount: value.length,
                  itemBuilder: (BuildContext context, int i){
                    return new GestureDetector(
                      onTap: () {
                        print(value[i]);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ProductDetailPage(data: value[i])),
                        );
                      },
                      child: Container(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                            value == [''] || value[i]['product_image'] == null
                                ? Image.network(
                              'http://style.anu.edu.au/_anu/4/images/placeholders/person.png',
                              height: 125,
                              width: 130,
                              fit: BoxFit.cover,
                            )
                            :Image.network(
                              'http://100.27.12.26:5000/resources/' +
                                  value[i]['product_image'],
                              height: 125,
                              width: 130,
                              fit: BoxFit.cover,
                            ),
                            SizedBox(height: 4.0,),
                            Text('\$\34.00'),
                            SizedBox(height: 4.0,),
                            Text('Woman T-Shirt'),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
